import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:todo/constants/DesignSystemColors.dart';
import 'package:todo/presentation/widgets/Month.dart';

import '../widgets/HamburgerMenu.dart';

class CalendarPage extends StatefulWidget {
  const CalendarPage({super.key});

  @override
  State<CalendarPage> createState() => _CalendarPageState();
}

class _CalendarPageState extends State<CalendarPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        backgroundColor: primaryColor,
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: whiteColor,
          ),
          onTap: () {
            // BlocProvider.of<MenuCubit>(context).changeSelectedMenuItem("Home");
            Get.back();
          },
        ),
        actions: [
          Container(
            width: MediaQuery.of(context).size.width / 3,
            padding: EdgeInsets.only(right: 0, bottom: 12),
            child: TextField(
              textAlignVertical: TextAlignVertical.bottom,
              textAlign: TextAlign.end,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.only(bottom: 10),
              ),
              style: TextStyle(
                  color: whiteColor, fontWeight: FontWeight.bold, fontSize: 18),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16.0, top: 3, left: 5),
            child: Icon(
              Icons.calendar_month,
              size: 25,
            ),
          ),
        ],
        title: Text(
          "Calendar",
          style: TextStyle(color: whiteColor),
        ),
      ),
      body: ListView(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    child: Icon(
                      Icons.arrow_back,
                      color: primaryColor,
                      size: 30,
                    ),
                    onTap: () {},
                  ),
                  Text(
                    "September 1999",
                    style: TextStyle(
                        fontSize: 20,
                        color: primaryColor,
                        fontWeight: FontWeight.bold),
                  ),
                  InkWell(
                    child: Icon(
                      Icons.arrow_forward,
                      color: primaryColor,
                      size: 30,
                    ),
                    onTap: () {},
                  )
                ],
              ),
              Container(
                padding: EdgeInsets.only(top: 30),
                width: MediaQuery.of(context).size.width - 100,
                height: MediaQuery.of(context).size.height - 160,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [Month(context, DateTime.now())],
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}
