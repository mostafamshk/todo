// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:todo/constants/Days.dart';
import 'package:todo/constants/categoryIcons.dart';

import 'Category.dart';

class Todo {
  static int idCounter = 0;
  int id;
  String title;
  String description;
  DateTime date;
  bool isRepeating;
  List<Days> repeatedDays;
  List<DateTime> isDone;
  Category category;
  Todo(
      {required this.id,
      required this.title,
      required this.description,
      required this.date,
      required this.isRepeating,
      required this.repeatedDays,
      required this.isDone,
      required this.category});

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'title': title,
      'description': description,
      'date': date.toString().split(" "),
      'isRepeating': isRepeating,
      'repeatedDays': repeatedDays.map((e) => e.index).toList(),
      'isDone': isDone.map((e) => e.toString().split(" ")).toList(),
      'category': category.toMap()
    };
  }

  factory Todo.fromMap(Map<String, dynamic> map) {
    return Todo(
        id: map["id"] as int,
        title: map["title"] as String,
        description: map["description"] as String,
        date: DateTime(
            int.parse(map["date"].split("-")[0]),
            int.parse(map["date"].split("-")[1]),
            int.parse(map["date"].split("-")[2]),
            23,
            59,
            59),
        isRepeating: map["isRepeating"] as bool,
        repeatedDays: map["repeatedDays"].map((e) {
          return Days.values.elementAt(e);
        }).toList() as List<Days>,
        isDone: map["isDone"].map((e) {
          return DateTime(
              int.parse(e.split("-")[0]),
              int.parse(e.split("-")[1]),
              int.parse(e.split("-")[2]),
              23,
              59,
              59);
        }) as List<DateTime>,
        category: Category.fromMap(map["category"]));
  }

  String toJson() => json.encode(toMap());

  factory Todo.fromJson(String source) =>
      Todo.fromMap(json.decode(source) as Map<String, dynamic>);
}
