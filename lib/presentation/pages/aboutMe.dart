import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:todo/constants/DesignSystemColors.dart';

import '../widgets/HamburgerMenu.dart';

class AboutMePage extends StatefulWidget {
  const AboutMePage({super.key});

  @override
  State<AboutMePage> createState() => _AboutMePageState();
}

class _AboutMePageState extends State<AboutMePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text(
          "About Us",
          style: TextStyle(color: whiteColor),
        ),
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: whiteColor,
          ),
          onTap: () {
            // BlocProvider.of<MenuCubit>(context).changeSelectedMenuItem("Home");
            Get.back();
          },
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(left: 30, right: 30, top: 0, bottom: 100),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  Icons.group,
                  color: primaryColor,
                  size: 100,
                ),
                Text(
                  "Man, Woman, Freedom is a powerfull team that has just started working and is growing insanely.Our main concern is simply preventing the world from using garbage apps. instead, we offer them our Masterpiece!",
                  style: TextStyle(
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(
                  Icons.email,
                  color: primaryColor,
                  size: 80,
                ),
                Text(
                  "manwomanfreedom@gmail.com",
                  style: TextStyle(
                      color: primaryColor,
                      fontWeight: FontWeight.bold,
                      fontSize: 20),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
