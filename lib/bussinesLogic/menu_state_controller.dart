import 'dart:convert';

import 'package:get/get.dart';

// ignore_for_file: public_member_api_docs, sort_constructors_first
class MenuStateController extends GetxController {
  RxString selected = 'home'.obs;

  void changeSelectedPage(String selected) {
    this.selected = selected.obs;
  }
}
