import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../../constants/DesignSystemColors.dart';
import '../../data/models/Category.dart';
import '../../data/models/Todo.dart';
import '../widgets/TodoITem.dart';

class SingleDayPage extends StatefulWidget {
  const SingleDayPage({super.key});

  @override
  State<SingleDayPage> createState() => _SingleDayPageState();
}

class _SingleDayPageState extends State<SingleDayPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        backgroundColor: primaryColor,
        leading: InkWell(
          child: Icon(
            Icons.arrow_back,
            color: whiteColor,
          ),
          onTap: () {
            Get.back();
          },
        ),
        title: Text(
          "Sat, 2 September 2022",
          style: TextStyle(color: whiteColor),
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        child: ListView(
          children: [
            TodoItem(
                Todo(
                    id: 0,
                    title: "HomeWork",
                    description: "Math Practices",
                    isDone: [],
                    isRepeating: false,
                    repeatedDays: [],
                    date: DateTime.now(),
                    category:
                        Category(name: "School", icon: Icon(Icons.school))),
                context,
                DateTime.now().add(Duration(days: 1))),
            TodoItem(
                Todo(
                    id: 0,
                    title: "HomeWork",
                    description: "Math Practices",
                    isDone: [],
                    isRepeating: false,
                    repeatedDays: [],
                    date: DateTime.now(),
                    category:
                        Category(name: "School", icon: Icon(Icons.school))),
                context,
                DateTime.now().add(Duration(days: 1))),
            TodoItem(
                Todo(
                    id: 0,
                    title: "HomeWork",
                    description: "Math Practices",
                    isDone: [],
                    isRepeating: false,
                    repeatedDays: [],
                    date: DateTime.now(),
                    category:
                        Category(name: "School", icon: Icon(Icons.school))),
                context,
                DateTime.now().add(Duration(days: 1))),
            TodoItem(
                Todo(
                    id: 0,
                    title: "HomeWork",
                    description: "Math Practices",
                    isDone: [],
                    isRepeating: false,
                    repeatedDays: [],
                    date: DateTime.now(),
                    category:
                        Category(name: "School", icon: Icon(Icons.school))),
                context,
                DateTime.now().add(Duration(days: 1))),
            TodoItem(
                Todo(
                    id: 0,
                    title: "HomeWork",
                    description: "Math Practices",
                    isDone: [],
                    isRepeating: false,
                    repeatedDays: [],
                    date: DateTime.now(),
                    category:
                        Category(name: "School", icon: Icon(Icons.school))),
                context,
                DateTime.now().add(Duration(days: 1))),
            TodoItem(
                Todo(
                    id: 0,
                    title: "HomeWork",
                    description: "Math Practices",
                    isDone: [],
                    isRepeating: false,
                    repeatedDays: [],
                    date: DateTime.now(),
                    category:
                        Category(name: "School", icon: Icon(Icons.school))),
                context,
                DateTime.now().add(Duration(days: 1))),
          ],
        ),
      ),
    );
  }
}
