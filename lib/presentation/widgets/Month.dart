import 'package:flutter/material.dart';
import 'package:todo/constants/Days.dart';
import 'package:todo/constants/DesignSystemColors.dart';

Widget Month(BuildContext context, DateTime dateTime) {
  List<DateTime> getDays() {
    List<DateTime> resultDays = [];
    DateTime day = dateTime.subtract(Duration(days: dateTime.day - 1));

    while (day.month == dateTime.month) {
      resultDays.add(day);
      day = day.add(Duration(days: 1));
    }

    return resultDays;
  }

  return Container(
      width: MediaQuery.of(context).size.width - 100,
      child: Wrap(
        alignment: WrapAlignment.center,
        runSpacing: 6,
        spacing: 6,
        children: getDays().map((day) {
          return InkWell(
            child: Container(
              width: 70,
              height: 70,
              decoration: BoxDecoration(
                  color: whiteColor,
                  border: Border.all(color: primaryColor, width: 1),
                  shape: BoxShape.circle),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      day.day.toString(),
                      style: TextStyle(color: primaryColor),
                    ),
                    Text(
                      DaysList[(day.weekday + 1) % 7],
                      style: TextStyle(color: primaryColor),
                    )
                  ],
                ),
              ),
            ),
            onTap: () {},
          );
        }).toList(),
      ));
}
