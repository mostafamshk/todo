import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:todo/constants/DesignSystemColors.dart';

import '../../data/models/Todo.dart';

Widget TodoItem(Todo todo, BuildContext context, DateTime date) {
  int status = getTodoStatus(todo, date);
  return Padding(
    padding: const EdgeInsets.only(bottom: 10.0),
    child: Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(0),
      decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          color: status == 0
              ? primaryBackgroundColor
              : status == 1
                  ? secondaryBackgroundColor
                  : errorBackgroundColor, //dynamic based on todo status
          border: Border.all(
              width: 1,
              color: status == 0
                  ? primaryColor
                  : status == 1
                      ? secondaryColor
                      : errorColor), //dynamic based on todo status
          borderRadius: BorderRadius.circular(10)),
      child: ListTile(
        leading: Container(
          width: 55,
          height: 55,
          decoration: BoxDecoration(
              border: Border.all(
                  color: status == 0
                      ? primaryColor
                      : status == 1
                          ? secondaryColor
                          : errorColor,
                  width: 1), //dynamic based on todo status
              borderRadius: BorderRadius.circular(100)),
          child: CircleAvatar(
            backgroundColor: whiteColor,
            child: Icon(
              todo.category.icon.icon, //dynamic based on todo status
              color: status == 0
                  ? primaryColor
                  : status == 1
                      ? secondaryColor
                      : errorColor,
              size: 35, //dynamic based on todo status
            ),
          ),
        ),
        title: Text("${todo.title}"),
        subtitle: Text("${todo.description}"),
        trailing: SizedBox(
          width: 75,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () {
                  Get.toNamed("/form");
                },
                child: Icon(
                  Icons.edit,
                  color: blackColor,
                ),
              ),
              InkWell(
                onTap: () {
                  //have to send and bloc action to make todo done!
                },
                child: Checkbox(
                  checkColor: whiteColor,
                  activeColor: secondaryColor,
                  value: true,
                  onChanged: (value) {},
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}

int getTodoStatus(Todo todo, DateTime date) {
  bool isDone = false;

  for (int i = 0; i < todo.isDone.length; i++) {
    DateTime doneTodoDateTime = todo.isDone.elementAt(i);
    if (doneTodoDateTime.day == date.day &&
        doneTodoDateTime.month == date.month &&
        doneTodoDateTime.year == date.year) {
      isDone = true;
      break;
    }
  }
  return isDone
      ? 1
      : date.isBefore(DateTime.now())
          ? 2
          : 0;
}
