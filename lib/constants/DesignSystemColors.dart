import 'package:flutter/material.dart';

final primaryColor = Color.fromARGB(255, 38, 64, 21);
final primaryBackgroundColor = Color.fromARGB(255, 184, 195, 176);

final secondaryColor = Color.fromARGB(255, 12, 167, 137);
final secondaryBackgroundColor = Color.fromARGB(255, 198, 242, 224);

final errorColor = Color.fromARGB(255, 218, 0, 0);
final errorBackgroundColor = Color.fromARGB(255, 255, 226, 240);

final whiteColor = Colors.white;
final blackColor = Colors.black;
