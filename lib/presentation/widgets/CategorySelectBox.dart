import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:todo/constants/DesignSystemColors.dart';

// import '../../bussinesLogic/dataStates/categories/categories_cubit.dart';
import '../../bussinesLogic/dataStateControllers/categories/categories_state_controllers.dart';
import '../../data/models/Category.dart';

class CategorySelectField extends StatefulWidget {
  bool isForm;
  CategorySelectField({super.key, required this.isForm});

  @override
  State<CategorySelectField> createState() => _CategorySelectFieldState();
}

class _CategorySelectFieldState extends State<CategorySelectField> {
  final CategoriesStateController categoriesStateController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(right: 8.0, left: 8, top: 4),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            widget.isForm
                ? Text(
                    "Category",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 102, 102, 102)),
                  )
                : Container(),
            Obx(
              () {
                return DropdownButton<Category>(
                  dropdownColor: widget.isForm ? whiteColor : primaryColor,
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: widget.isForm ? primaryColor : whiteColor,
                  ),
                  value: categoriesStateController.categories.elementAt(0),
                  items: categoriesStateController.categories
                      .map(
                        (e) => DropdownMenuItem<Category>(
                            value: e,
                            child: SizedBox(
                              // width: 120,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Icon(
                                    e.icon.icon,
                                    color: widget.isForm
                                        ? primaryColor
                                        : whiteColor,
                                    size: 20,
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Text(
                                    e.name,
                                    style: TextStyle(
                                        color: widget.isForm
                                            ? primaryColor
                                            : whiteColor,
                                        fontWeight: FontWeight.bold),
                                  )
                                ],
                              ),
                            )),
                      )
                      .toList(),
                  onChanged: (value) {},
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
