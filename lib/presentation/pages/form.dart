import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:todo/constants/Days.dart';
import 'package:todo/constants/DesignSystemColors.dart';
import '../widgets/CategorySelectBox.dart';

class FormPage extends StatefulWidget {
  const FormPage({super.key});

  @override
  State<FormPage> createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  final GlobalKey<FormState> _key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        leading: InkWell(
            child: Icon(
              Icons.arrow_back,
              color: whiteColor,
            ),
            onTap: () {
              Get.back();
            }),
        title: Text(
          "Add a Todo",
          style: TextStyle(color: whiteColor),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Text("Add"),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
        width: MediaQuery.of(context).size.width,
        child: Form(
          key: _key,
          child: ListView(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: TextField(
                  decoration: InputDecoration(
                    labelText: "Title",
                  ),
                  style: TextStyle(
                      color: primaryColor, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: TextField(
                  decoration: InputDecoration(
                    labelText: "Description",
                  ),
                  style: TextStyle(
                      color: primaryColor, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: TextField(
                  decoration: InputDecoration(
                    labelText: "Date",
                  ),
                  style: TextStyle(
                      color: primaryColor, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Repeat Every:",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 102, 102, 102)),
                        ),
                        Checkbox(
                          value: true,
                          onChanged: (value) {},
                          checkColor: whiteColor,
                          activeColor: primaryColor,
                        ),
                      ],
                    ),
                    Wrap(
                      spacing: 10,
                      crossAxisAlignment: WrapCrossAlignment.center,
                      verticalDirection: VerticalDirection.down,
                      runSpacing: 10,
                      children: [0, 1, 2, 3, 4, 5, 6].map((d) {
                        Days day = Days.values[d];
                        return InkWell(
                          onTap: () {},
                          child: Container(
                            width: 50,
                            height: 50,
                            child: Center(
                              child: Text(
                                day.name,
                                style: TextStyle(color: primaryColor),
                              ),
                            ),
                            decoration: BoxDecoration(
                                border:
                                    Border.all(width: 1, color: primaryColor),
                                color: whiteColor,
                                shape: BoxShape.circle),
                          ),
                        );
                      }).toList(),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 30.0),
                child: SizedBox(
                  child: CategorySelectField(
                    isForm: true,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
