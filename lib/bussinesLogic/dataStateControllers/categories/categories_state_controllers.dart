// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:todo/data/models/Todo.dart';

import '../../../../data/models/Category.dart';

class CategoriesStateController extends GetxController {
  RxList<Category> categories = Category.createCategoriesList().obs;
}
