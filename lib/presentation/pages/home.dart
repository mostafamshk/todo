import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:todo/bussinesLogic/dataStateControllers/categories/categories_state_controllers.dart';
import 'package:todo/constants/DesignSystemColors.dart';
import 'package:todo/presentation/widgets/CategorySelectBox.dart';
import 'package:todo/presentation/widgets/HamburgerMenu.dart';
import 'package:todo/presentation/widgets/SeperatorWithDate.dart';
import 'package:todo/presentation/widgets/TodoITem.dart';

import '../../data/models/Category.dart';
import '../../data/models/Todo.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: whiteColor,
        appBar: AppBar(
          title: Text("Todo List"),
          backgroundColor: primaryColor,
          actions: [
            CategorySelectField(
              isForm: false,
            )
          ],
        ),
        drawer: HamburgerMenu(),
        floatingActionButton: FloatingActionButton(
          child: Center(
            child: Icon(Icons.add),
          ),
          onPressed: () {
            Get.toNamed('/form');
          },
        ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
          child: ListView(
            children: [
              SeperatorWithDate(DateTime.now().add(Duration(days: 1)), true,
                  true, primaryColor),
              TodoItem(
                  Todo(
                      id: 0,
                      title: "HomeWork",
                      description: "Math Practices",
                      isDone: [],
                      isRepeating: false,
                      repeatedDays: [],
                      date: DateTime.now(),
                      category:
                          Category(name: "School", icon: Icon(Icons.school))),
                  context,
                  DateTime.now().add(Duration(days: 1))),
              TodoItem(
                  Todo(
                      id: 0,
                      title: "HomeWork",
                      description: "Math Practices",
                      isDone: [],
                      isRepeating: false,
                      repeatedDays: [],
                      date: DateTime.now(),
                      category:
                          Category(name: "School", icon: Icon(Icons.school))),
                  context,
                  DateTime.now().add(Duration(days: 1))),
              TodoItem(
                  Todo(
                      id: 0,
                      title: "HomeWork",
                      description: "Math Practices",
                      isDone: [],
                      isRepeating: false,
                      repeatedDays: [],
                      date: DateTime.now(),
                      category:
                          Category(name: "School", icon: Icon(Icons.school))),
                  context,
                  DateTime.now().add(Duration(days: 1))),
              SeperatorWithDate(DateTime.now().add(Duration(days: 1)), true,
                  true, primaryColor),
              TodoItem(
                  Todo(
                      id: 0,
                      title: "HomeWork",
                      description: "Math Practices",
                      isDone: [],
                      isRepeating: false,
                      repeatedDays: [],
                      date: DateTime.now(),
                      category:
                          Category(name: "School", icon: Icon(Icons.school))),
                  context,
                  DateTime.now().add(Duration(days: 1))),
              TodoItem(
                  Todo(
                      id: 0,
                      title: "HomeWork",
                      description: "Math Practices",
                      isDone: [],
                      isRepeating: false,
                      repeatedDays: [],
                      date: DateTime.now(),
                      category:
                          Category(name: "School", icon: Icon(Icons.school))),
                  context,
                  DateTime.now().add(Duration(days: 1))),
              TodoItem(
                  Todo(
                      id: 0,
                      title: "HomeWork",
                      description: "Math Practices",
                      isDone: [],
                      isRepeating: false,
                      repeatedDays: [],
                      date: DateTime.now(),
                      category:
                          Category(name: "School", icon: Icon(Icons.school))),
                  context,
                  DateTime.now().add(Duration(days: 1))),
            ],
          ),
        ));
  }
}
