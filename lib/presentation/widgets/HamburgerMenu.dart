import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';
import 'package:todo/constants/DesignSystemColors.dart';

import '../../bussinesLogic/menu_state_controller.dart';

class HamburgerMenu extends StatefulWidget {
  const HamburgerMenu({super.key});

  @override
  State<HamburgerMenu> createState() => _HamburgerMenuState();
}

class _HamburgerMenuState extends State<HamburgerMenu> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 140,
      backgroundColor: primaryColor,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildMenuItems(context),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) {
    return Container();
  }

  Widget buildMenuItems(BuildContext context) {
    final MenuStateController menuStateController = Get.find();
    return Container(
      height: MediaQuery.of(context).size.height,
      padding: EdgeInsets.all(10),
      child: GetBuilder<MenuStateController>(
        builder: (_) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: InkWell(
                      onTap: () {
                        Get.back();
                      },
                      child: Icon(
                        Icons.menu,
                        size: 30,
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: InkWell(
                      onTap: () {
                        menuStateController.changeSelectedPage("calendar");
                        Get.toNamed('/calendar');
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.calendar_month,
                            size: 30,
                            color: menuStateController.selected == "calendar"
                                ? Theme.of(context).colorScheme.onBackground
                                : Theme.of(context).colorScheme.onPrimary,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            "Calendar",
                            style: TextStyle(
                                color: whiteColor, fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 16.0),
                    child: InkWell(
                      onTap: () {
                        menuStateController.changeSelectedPage("aboutMe");
                        Get.toNamed('/aboutMe');
                      },
                      child: Row(
                        children: [
                          Icon(
                            Icons.info,
                            size: 30,
                            color: menuStateController.selected == "About Me"
                                ? Theme.of(context).colorScheme.onBackground
                                : Theme.of(context).colorScheme.onPrimary,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text("About Us",
                              style: TextStyle(
                                  color: whiteColor,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              InkWell(
                child: Icon(
                  Icons.exit_to_app,
                  color: whiteColor,
                  size: 30,
                ),
                onTap: () {
                  SystemNavigator.pop();
                },
              )
            ],
          );
        },
      ),
    );
  }
}
