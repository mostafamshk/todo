import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:todo/bussinesLogic/pagesStates/aboutMe/bindings.dart';
import 'package:todo/bussinesLogic/pagesStates/calendar/bindings.dart';
import 'package:todo/bussinesLogic/pagesStates/form/bindings.dart';
import 'package:todo/bussinesLogic/pagesStates/home/bindings.dart';
import 'package:todo/bussinesLogic/pagesStates/singleDay/bindings.dart';
import 'package:todo/presentation/pages/aboutMe.dart';
import 'package:todo/presentation/pages/calendar.dart';
import 'package:todo/presentation/pages/form.dart';
import 'package:todo/presentation/pages/home.dart';
import 'package:todo/presentation/pages/singleDay.dart';

class AppRoutes {
  List<GetPage<dynamic>> getAppRoutes() {
    return [
      GetPage(name: '/home', page: () => HomePage(), binding: HomeBindings()),
      GetPage(name: '/form', page: () => FormPage(), binding: FormBindings()),
      GetPage(name: '/calendar', page: () => CalendarPage(), binding: CalendarBindings()),
      GetPage(name: '/aboutMe', page: () => AboutMePage(), binding: AboutMeBindings()),
      GetPage(name: '/singleDay', page: () => SingleDayPage(), binding: SingleDayBindings() , ),
    ];
  }
}
