import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../constants/Days.dart';
import '../../constants/Months.dart';
import '../../constants/DesignSystemColors.dart';

Widget SeperatorWithDate(DateTime date, bool isDay,
    bool translateToYesterDayTodayTomorrow, Color color) {
  return Container(
    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          dateStringFormatter(date, isDay, translateToYesterDayTodayTomorrow),
          style: TextStyle(
              color: color, fontWeight: FontWeight.bold, fontSize: 16),
        ),
        Divider(
          height: 2,
          thickness: 5,
          color: color,
        )
      ],
    ),
  );
}

String dateStringFormatter(
    DateTime date, bool isDay, bool translateToYesterDayTodayTomorrow) {
  String result = "";
  DateTime today = DateTime.now();
  DateTime yesterday = DateTime.now().subtract(Duration(days: 1));
  DateTime tomorrow = DateTime.now().add(Duration(days: 1));

  result = translateToYesterDayTodayTomorrow
      ? (today.day == date.day &&
              today.month == date.month &&
              today.year == date.year)
          ? "Today"
          : (yesterday.day == date.day &&
                  yesterday.month == date.month &&
                  yesterday.year == date.year)
              ? "Yesterday"
              : (tomorrow.day == date.day &&
                      tomorrow.month == date.month &&
                      tomorrow.year == date.year)
                  ? "Tomorrow"
                  : ""
      : isDay
          ? "${Days.values[date.weekday - 1]}, ${date.day} ${Months.values[date.month - 1]} ${date.year}"
          : "${Months.values[date.month - 1]} ${date.year}";

  return result;
}
