import 'package:flutter/material.dart';

final List<String> CATEGORIES_NAMES = [
  "All",
  "Work",
  "School",
  "Home",
  "Hobbies",
  "Payments",
  "Family",
  "Friends",
  "Work Out"
];

final List<IconData> CATEGORIES_ICON_DATA = [
  Icons.task,
  Icons.work,
  Icons.school,
  Icons.home,
  Icons.hub,
  Icons.payment,
  Icons.family_restroom,
  Icons.people,
  Icons.sports_gymnastics
];
