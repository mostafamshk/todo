import 'package:get/get.dart';
import 'package:todo/bussinesLogic/dataStateControllers/categories/categories_state_controllers.dart';
import 'package:todo/bussinesLogic/dataStateControllers/todos/todos_state.dart';
import 'package:todo/bussinesLogic/menu_state_controller.dart';
import 'package:todo/data/models/Category.dart';

import '../../data/models/Todo.dart';

class InitialBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TodosStateController>(() => TodosStateController());
    Get.lazyPut(() => CategoriesStateController());
    Get.lazyPut<MenuStateController>(() => MenuStateController());
  }
}
