// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:get/get.dart';

import '../../../data/models/Todo.dart';

class TodosStateController extends GetxController {
  RxList<Todo> todos = [].obs as RxList<Todo>;
  int todoIdCounter = 0;

  void addTodo(Todo todo) {
    todos.add(todo);
    todoIdCounter++;
  }

  void updateTodo(Todo todo, int index) {
    todos[index] = todo;
  }

  void deleteTodo(Todo todo, int index) {
    todos.removeAt(index);
  }
}
