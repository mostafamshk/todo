// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:todo/constants/categoryIcons.dart';

class Category {
  String name;
  Icon icon;
  Category({
    required this.name,
    required this.icon,
  });

  static List<Category> createCategoriesList() {
    return [
      Category(name: "All", icon: Icon(Icons.task)),
      Category(name: "Work", icon: Icon(Icons.work)),
      Category(name: "School", icon: Icon(Icons.school)),
      Category(name: "Home", icon: Icon(Icons.home)),
      Category(name: "Hobbies", icon: Icon(Icons.hub)),
      Category(name: "Payments", icon: Icon(Icons.payment)),
      Category(name: "Family", icon: Icon(Icons.family_restroom)),
      Category(name: "Friends", icon: Icon(Icons.people)),
      Category(name: "Work Out", icon: Icon(Icons.sports_gymnastics)),
    ];
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'icon': CATEGORIES_NAMES.indexOf(name),
    };
  }

  factory Category.fromMap(Map<String, dynamic> map) {
    return Category(
      name: map['name'] as String,
      icon: Icon(CATEGORIES_ICON_DATA.elementAt(map['icon'])),
    );
  }

  String toJson() => json.encode(toMap());

  factory Category.fromJson(String source) =>
      Category.fromMap(json.decode(source) as Map<String, dynamic>);
}
